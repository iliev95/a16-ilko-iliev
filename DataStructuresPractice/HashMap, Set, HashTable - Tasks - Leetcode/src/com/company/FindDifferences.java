package com.company;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class FindDifferences {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        String t = scanner.nextLine();
        String [] str = s.split("");
        Set<String> setString = new HashSet<>(Arrays.asList(str));
        String[] myEl = t.split("");
        for (int i = 0; i <myEl.length ; i++) {
            if (!setString.contains(myEl[i])) {
                System.out.println(myEl[i]);
                break;
            }
        }
    }
}
