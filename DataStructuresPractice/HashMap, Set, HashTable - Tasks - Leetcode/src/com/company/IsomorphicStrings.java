package com.company;

import java.util.HashMap;
import java.util.Scanner;

public class IsomorphicStrings {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String firstInput = scanner.nextLine();
        String secondInput = scanner.nextLine();
        String [] firstArray = firstInput.split("");
        String [] secondArray = secondInput.split("");
        HashMap<String,String> map = new HashMap<>();
        boolean result = true;

        for (int i = 0; i <firstArray.length; i++) {
            if(map.containsKey(firstArray[i])) {
                if (!map.get(firstArray[i]).equalsIgnoreCase(secondArray[i]) ) {
                    result = false;
                    break;
                }
            } else {
                if (map.containsValue(secondArray[i])) {
                    result = false;
                } else {
                    map.put(firstArray[i], secondArray[i]);
                }
            }

        }
        System.out.println(result);
    }
}
