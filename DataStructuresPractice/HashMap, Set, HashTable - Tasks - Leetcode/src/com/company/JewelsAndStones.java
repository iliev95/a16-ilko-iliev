package com.company;

import java.util.*;

public class JewelsAndStones {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String jewels = scanner.nextLine();

        String stones = scanner.nextLine();
        char [] char2 = stones.toCharArray();
        Set<Character> jewels2 = new HashSet<>();

        for (int i = 0; i < jewels.length() ; i++) {
            char symbol = jewels.charAt(i);
            jewels2.add(symbol);
        }

        int counter = 0;

        for (int i = 0; i <char2.length; i++) {
            char symbol = char2[i];
            if(jewels2.contains(symbol)) {
                counter++;
            }
        }

        System.out.println(counter);
    }
}
