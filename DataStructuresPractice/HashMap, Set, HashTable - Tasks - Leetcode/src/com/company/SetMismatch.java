package com.company;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class SetMismatch {

    public static int [] getValues(String input){
        String [] str = input.split(",");
        int [] result = new int [str.length];
        for (int i = 0; i <str.length ; i++) {
            result[i] = Integer.parseInt(str[i]);
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        int [] array = getValues(input);
        Set<Integer> valueSet = new HashSet<>();
        int [] result = new int[2];

        int counter  = 1;

        for (int i = 0; i <array.length; i++) {

            if(valueSet.contains(array[i])) {
                result[0] = array[i];
            } else {
                valueSet.add(array[i]);
            }

            if (!valueSet.contains(counter)) {
                result[1] = counter;
            }
            counter++;
        }

        for (int i = 0; i <result.length; i++) {
            System.out.printf("%d ", result[i]);
        }

    }
}
