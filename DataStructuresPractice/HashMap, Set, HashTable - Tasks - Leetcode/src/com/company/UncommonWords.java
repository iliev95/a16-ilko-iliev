package com.company;

import java.util.*;

public class UncommonWords {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String firstSentence = scanner.nextLine();
        String secondSentence = scanner.nextLine();
        String [] firstArray = firstSentence.split(" ");
        String[] secondArray = secondSentence.split(" ");
        HashMap<String,Integer> firstMap= new HashMap<>();
        HashMap<String,Integer> secondMap = new HashMap<>();
        List<String> result = new ArrayList<>();

        for (int i = 0; i <firstArray.length ; i++) {

            if (firstMap.containsKey(firstArray[i])){
                firstMap.put(firstArray[i],firstMap.get(firstArray[i])+1);
            } else {
                firstMap.putIfAbsent(firstArray[i], 1);
            }
        }

        for (int i = 0; i < secondArray.length ; i++) {
            if (secondMap.containsKey(secondArray[i])){
                secondMap.put(secondArray[i],secondMap.get(secondArray[i])+1);
            } else {
                secondMap.putIfAbsent(secondArray[i], 1);
            }
        }

        for (Map.Entry<String, Integer> entry : firstMap.entrySet()) {
            if(!secondMap.containsKey(entry.getKey()) && entry.getValue()==1) {
                result.add(entry.getKey());
            }
        }

        for (Map.Entry<String, Integer> entry : secondMap.entrySet()) {
            if(!firstMap.containsKey(entry.getKey()) && entry.getValue()==1) {
                result.add(entry.getKey());
            }
        }

        System.out.println(result);
    }
}
