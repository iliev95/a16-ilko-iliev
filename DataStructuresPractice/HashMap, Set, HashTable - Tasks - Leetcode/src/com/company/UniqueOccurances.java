package com.company;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class UniqueOccurances {
    public static int [] getValues(String input){
        String [] str = input.split(",");
        int [] result = new int [str.length];
        for (int i = 0; i <str.length ; i++) {
            result[i] = Integer.parseInt(str[i]);
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        int [] array = getValues(input);
        HashMap<Integer, Integer> occurances = new HashMap<>();
        boolean result = true;

        for (int i = 0; i < array.length; i++) {
            occurances.putIfAbsent(array[i], 1);
            if (occurances.containsKey(array[i])) {
                occurances.put(array[i], occurances.get(array[i])+1 );
            }
        }

        for (Map.Entry<Integer, Integer> entry : occurances.entrySet()) {
            int value = entry.getValue();
            occurances.put(entry.getKey(), 0);
            if (occurances.containsValue(value)) {
                result = false;
                break;
            }

        }

        System.out.println(result);

    }
}
