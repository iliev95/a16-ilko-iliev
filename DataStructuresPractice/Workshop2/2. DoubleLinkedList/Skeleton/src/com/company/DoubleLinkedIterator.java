package com.company;

import java.util.Iterator;
import java.util.List;

public class DoubleLinkedIterator<T> implements Iterator {
    private DoubleNode currentNode;

    public DoubleLinkedIterator(DoubleNode node){
        this.currentNode = node;
    }

    public T next (){
        T value = (T) currentNode.getValue();
        this.currentNode = currentNode.getNext();
        return value;
    }

    public boolean hasNext() {
        if (currentNode == null) {
            return false;
        }
        return true;
    }

    public DoubleNode getCurrentNode() {
        return currentNode;
    }
}
