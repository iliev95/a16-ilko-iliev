package com.company;


public interface DoublyLinkedList<T> extends Iterable<T> {
    DoubleNode getHead(); //ok
    DoubleNode getTail(); // ok
    int getCount(); //ok
    void addLast(T value); // ok
    void addFirst(T value); // ok
    void insertBefore(DoubleNode node, T value);
    void insertAfter(DoubleNode node, T value);
    T removeFirst(); // ok
    T removeLast(); //ok
    DoubleNode find(T value);// ok
}
