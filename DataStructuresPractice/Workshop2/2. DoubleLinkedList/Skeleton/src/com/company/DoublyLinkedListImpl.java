package com.company;

import java.util.Iterator;
import java.util.List;

public class DoublyLinkedListImpl<T> implements DoublyLinkedList<T> {
    private DoubleNode<T> firstElement;
    private DoubleNode<T> lastElement;
    private int count = 0;

    public DoublyLinkedListImpl(){

    }

    public DoublyLinkedListImpl(List<T> values){
        for (T value : values) {
            addLast(value);
        }
    }

    @Override
    public void addFirst(T value) {
        DoubleNode newNode = new DoubleNode(value);
        if (count == 0) {
            this.firstElement = newNode;
            this.lastElement = newNode;
        }else {
            firstElement.setPrev(newNode);
            newNode.setNext(firstElement);
            firstElement = newNode;
        }
        count++;
    }

    @Override
    public void addLast(T value) {
        DoubleNode newNode = new DoubleNode(value);
        if (count == 0) {
            this.firstElement = newNode;
            this.lastElement = newNode;
        }else {
            lastElement.setNext(newNode);
            newNode.setPrev(lastElement);
            lastElement = newNode;
        }
        count++;
    }

    @Override
    public DoubleNode getHead() {
        return this.firstElement;
    }

    @Override
    public DoubleNode getTail() {
        return this.lastElement;
    }

    @Override
    public T removeFirst() {
        T element  = (T) firstElement.getValue();
        if (getCount() != 1) {
            firstElement =  firstElement.getNext();
            firstElement.setPrev(null);
        }
        count--;
        return element;
    }

    @Override
    public T removeLast() {
        T element = (T) lastElement.getValue();
        if (getCount() != 1) {
            lastElement = lastElement.getPrev();
            lastElement.setNext(null);
        }
        count--;
        return element;
    }

    @Override
    public DoubleNode find(T value) {
        if (getCount() == 0) {
            return null;
        }
        if (firstElement.getValue() == value) {
            return firstElement;
        }

        DoubleNode nextNode = firstElement.getNext();
        while(nextNode!= null) { // The iterator method of the class could have been used as well(with some rearrangements).
            if (nextNode.getValue()==value) {
                break;
            }
            nextNode = nextNode.getNext();
        }

        return nextNode;
    }

    @Override
    public void insertAfter(DoubleNode foundNode, T value) {
        DoubleNode newNode = new DoubleNode(value);
        if (foundNode == null) {
            throw new NullPointerException();
        }
        if (foundNode == lastElement) {
            addLast(value);
        } else {
            newNode.setPrev(foundNode);
            newNode.setNext(foundNode.getNext());
            newNode.getPrev().setNext(newNode);
            newNode.getNext().setPrev(newNode);
            count++;
        }
    }

    @Override
    public void insertBefore(DoubleNode foundNode, T value) {
        DoubleNode newNode = new DoubleNode(value);
        if (foundNode == null) {
            throw new NullPointerException();
        }
        if (foundNode == firstElement) {
            addFirst(value);
        } else {
            newNode.setNext(foundNode);
            newNode.setPrev(foundNode.getPrev());
            newNode.getNext().setPrev(newNode);
            newNode.getPrev().setNext(newNode);
            count++;
        }
    }

    @Override
    public Iterator<T> iterator() {
        Iterator<T>  iterator = new DoubleLinkedIterator<>(this.firstElement);
        return iterator;
    }

    @Override
    public int getCount() {
        return count;
    }
}
