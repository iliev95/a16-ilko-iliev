package com.company;

import java.util.List;

public class ListNode {
      private int val;
      private ListNode next;

      public ListNode(int x) {
          val = x;
      }

    public ListNode getNext() {
        return next;
    }

    public int getVal() {
        return val;
    }

    public void setNext(ListNode next) {
        this.next = next;
    }

    public void setVal(int val) {
        this.val = val;
    }
}
