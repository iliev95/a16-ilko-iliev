package com.company;

public class RemoveDuplcates {

    public ListNode deleteDuplicates(ListNode initial) { // here the head of the singly linked list is passed as parameter.
        ListNode node = initial;

        if (node.getNext() != null && node!= null){
            ListNode nextNode = node.getNext();
            if (node.getVal() == nextNode.getVal()){
                node.setNext(nextNode.getNext());
            }

            deleteDuplicates(nextNode);
        }
        return node;
    }
}
/* Valid code, that can be pasted in leetcode, if needed:

    ListNode node = initial;

        if (node.next != null && node!= null){
                ListNode nextNode = node.next;
                if (node.val == nextNode.val){
                node.next = nextNode.next;
                }

                deleteDuplicates(node.next);
                }
                return node;

 */