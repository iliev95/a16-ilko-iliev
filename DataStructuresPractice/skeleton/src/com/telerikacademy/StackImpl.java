package com.telerikacademy;

import java.util.*;

public class StackImpl<T> implements Stack<T> {
    private static final int DEFAULT =5;
    private Object [] array;
    private int stackSize;

    public StackImpl(){
        this.array = new Object [DEFAULT];
        this.stackSize = 0;
    }

    @Override
    public void push(T element) {

        if(stackSize == array.length) {
            resize();
        }
        array[stackSize]  = element;
        stackSize++;
    }

    @Override
    public T pop() {
        if (isEmpty()) {
            throw new IllegalArgumentException();
        }
        Object [] newArray = new Object[array.length -1];
        Object firstElement = array[stackSize - 1];
        System.arraycopy(array, 1, newArray, 0, array.length - 1);
            this.array = newArray;
            this.stackSize--;

            return (T) firstElement;
    }

    @Override
    public T peek() {
        if (isEmpty()) {
            throw new IllegalArgumentException();
        }
        Object obj = array[stackSize-1];

        return (T) obj;
    }

    @Override
    public int size() {
        return this.stackSize;
    }

    @Override
    public boolean isEmpty() {
        if(this.stackSize == 0) {
            return true;
        }
        return false;
    }

    private void resize (){
        array = Arrays.copyOf(array, array.length *2 );
    }
}
