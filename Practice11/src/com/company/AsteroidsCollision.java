package com.company;

import java.util.*;

public class AsteroidsCollision {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String [] array = input.split(", ");
        List<Integer> values = new ArrayList<>();
        for (int i = 0; i <array.length ; i++) {
            values.add(Integer.parseInt(array[i]));
        }

        for (int i = 0; i < values.size()-1; i++) {
            if (values.get(i) >0 &&  values.get(i+1) < 0) {
                if (values.get(i) == Math.abs(values.get(i+1))) {
                    values.remove(i);
                    values.remove(i);
                }else {
                    int setValue = 0;
                    if (values.get(i) > Math.abs(values.get(i+1))) {
                        setValue = values.get(i);
                    } else {
                        setValue = values.get(i+1);
                    }
                    values.set(i,setValue);
                    values.remove(i + 1);
                }
                i=-1;
            }
        }

        System.out.println(values);
    }
}
