package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

public class BackSpaceString {
    public static List<String> getValues(String input){
        String [] array = input.split("");
        List<String> values = new ArrayList<>();
        for (int i = 0; i <array.length ; i++) {
            values.add(array[i]);
        }
        return values;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String input1 = scanner.nextLine();
        String input2 = scanner.nextLine();

        List <String> firstStringValues = getValues(input1);
        List <String> secondStringValues = getValues(input2);

        boolean equals = true;


        for (int i = 0; i <firstStringValues.size(); i++) {
            if(firstStringValues.get(i).equalsIgnoreCase("#")) {
                firstStringValues.remove(i);
                if(i!=0) {
                    firstStringValues.remove(i-1);
                }
                i = -1;
            }
        }

        for (int i = 0; i <secondStringValues.size() ; i++) {
            if(secondStringValues.get(i).equalsIgnoreCase("#")) {
                secondStringValues.remove(i);
                if(i!=0) {
                    secondStringValues.remove(i-1);
                }
                i = -1;
            }
        }


        if (firstStringValues.size() != secondStringValues.size()) {
            equals = false;
        } else {
            for (int i = 0; i <firstStringValues.size() ; i++) {
                if (!firstStringValues.get(i).equalsIgnoreCase(secondStringValues.get(i))) {
                    equals = false;
                    break;
                }
            }
        }

        System.out.println(equals);
    }
}
