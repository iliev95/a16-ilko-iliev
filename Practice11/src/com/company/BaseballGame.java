package com.company;

import java.util.Scanner;
import java.util.Stack;

public class BaseballGame {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String [] commands = input.split(",");
        Stack<Integer> roundsPoints = new Stack<>();

        int overallPoints = 0;

        for (int i = 0; i <commands.length ; i++) {
            int nextRound = 0;
            String element = commands[i];
            char symbol = element.charAt(0);
             if (Character.isDigit(symbol) || symbol == '-') {
                 int value = Integer.parseInt(element);
                 overallPoints+= value;
                 roundsPoints.push(value);
             } else if (symbol == 'C') {
                 overallPoints-= roundsPoints.peek();
                 roundsPoints.pop();
             } else if (symbol == 'D') {
                 nextRound = 2 * roundsPoints.peek();
                 overallPoints+= nextRound;
                 roundsPoints.push(nextRound);
             } else if (symbol == '+') {
                 int num1 = roundsPoints.peek();
                 roundsPoints.pop();
                 int num2 = roundsPoints.peek();
                 nextRound+= num1 + num2;
                 overallPoints+= nextRound;
                 roundsPoints.push(num1);
                 roundsPoints.push(nextRound);
             }
        }
        System.out.println(overallPoints);
    }
}
