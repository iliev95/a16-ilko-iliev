package com.company;

import java.util.Scanner;
import java.util.Stack;

public class Lemonade {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String [] bills = input.split(",");
        Stack<Integer> available = new Stack();
        boolean possible = true;

        for (int i = 0; i <bills.length ; i++) {
            int currentBill = Integer.parseInt(bills[i]);
            int change = currentBill;
            while(change!=5) {
                if (available.isEmpty()) {
                    possible = false;
                    break;
                }
                int conv= change;
                change -= available.pop();

                if(change < 5) {
                    change = conv;
                }
            }
            available.push(currentBill);
        }
        System.out.println(possible);
    }
}
