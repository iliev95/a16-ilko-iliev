package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class asteroidsTask {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input1 = scanner.nextLine();
        String input2 = scanner.nextLine();
        String [] subArray = input1.split(",");
        String [] fullArray = input2.split(",");
        List<Integer> result = new ArrayList<>();

        int greater = 0;

        for (int i = 0; i <subArray.length ; i++) {
            int startingIndex = 0;
            int searchedNumber = Integer.parseInt(subArray[i]);
            for (int j = 0; j <fullArray.length ; j++) {
                int checkNumber = Integer.parseInt(fullArray[j]);
                if (checkNumber == searchedNumber) {
                    startingIndex = j;
                    break;
                }
            }
            for (int k = startingIndex; k <fullArray.length ; k++) {
                boolean found = false;
                greater = Integer.parseInt(fullArray[k]);
                if (greater > searchedNumber) {
                    result.add(greater);
                    found = true;
                    break;
                } else if (k == fullArray.length-1){
                    result.add(-1);
                }
            }
        }

        System.out.println(result);

    }
}
