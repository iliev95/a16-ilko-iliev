package com.telerikacademy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class QueueImpl<T> implements Queue<T> {
    private LinkedList<T> elements;

    public QueueImpl() {
        this.elements = new LinkedList<>();
    }

    @Override
    public void offer(T elem) {
        elements.add(elem);
    }

    @Override
    public T poll() {
        if (isEmpty()) {
            throw new IllegalArgumentException();
        }
        Object obj = elements.getFirst();
        elements.removeFirst();
        return (T) obj;
    }

    @Override
    public boolean isEmpty() {
        if (size() == 0) {
          return true;
        }
        return false;
    }

    @Override
    public T peek(){
        if (isEmpty()) {
            throw new IllegalArgumentException();
        }
        return elements.getFirst();
    }

    @Override
    public int size() {
        return elements.size();
    }
}
