package com.company;

public class Patient extends User{
    private boolean insured;

    public Patient(long id, String firstName, String lastName, String gender, String email, boolean insured ){
        super(id,firstName, lastName, gender, email);
        setInsured(insured);
    }

    public boolean isInsured() {
        return insured;
    }

    private void setInsured(boolean insured) {
        this.insured = insured;
    }
}
