package  com.company;

public class HealthInsurancePlan {
    private double coverage;
    private double discount;

    public HealthInsurancePlan(){

    }

    public HealthInsurancePlan(double coverage, double discount) {
        setCoverage(coverage);
        setDiscount(discount);
    }

    public double getCoverage() {
        return coverage;
    }
    public double getDiscount() {
        return discount;
    }

    private void setCoverage(double coverage) {
        this.coverage = coverage;
    }

    private void setDiscount(double discount) {
        this.discount = discount;
    }
}
