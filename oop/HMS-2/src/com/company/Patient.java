package com.company;

public class Patient extends User{
    private boolean insured;
    private HealthInsurancePlan insurancePlan;

    public Patient(long id, String firstName, String lastName, String gender, String email,
                   boolean insured){
        super(id,firstName, lastName, gender, email);
        setInsured(insured);
    }

    public Patient(long id, String firstName, String lastName, String gender, String email,
                   boolean insured, HealthInsurancePlan insurancePlan){
        super(id,firstName, lastName, gender, email);
        setInsured(insured);
        setInsurancePlan(insurancePlan);
    }

    public boolean isInsured() {
        return insured;
    }

    public HealthInsurancePlan getInsurancePlan() {
        return insurancePlan;
    }

    private void setInsurancePlan(HealthInsurancePlan insurancePlan) {
        this.insurancePlan = insurancePlan;
    }

    private void setInsured(boolean insured) {
        this.insured = insured;
    }
}
