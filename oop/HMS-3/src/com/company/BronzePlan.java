package com.company;

public class BronzePlan extends HealthInsurancePlan{

    public BronzePlan(){
        super(0.6,25);
    }

    @Override
    public double computeMonthlyPremium(double salary) {
        double premium = salary * 0.05;
        return 0;
    }
}
