package com.company;

public class Doctor extends Staff {
    private String specialisation;

    public Doctor(long id,String firstName, String lastName,
                  String gender, String email, int yearsOfExperience, String description, double salary,
                  String specialisation, boolean insured, HealthInsurancePlan insurancePlan){

        super(id,firstName, lastName, gender, email,yearsOfExperience, description, salary, insured, insurancePlan);
        setSpecialisation(specialisation);
    }

    public String getSpecialisation() {
        return specialisation;
    }

    private void setSpecialisation(String specialisation){
        this.specialisation = specialisation;
    }
}
