package com.company;

public class GoldPlan extends HealthInsurancePlan{

    public GoldPlan (){
        super(0.8,40);
    }

    @Override
    public double computeMonthlyPremium(double salary) {
        double premium = 0.07 * salary;
        return premium;
    }
}
