package  com.company;

public abstract class HealthInsurancePlan {
    private double coverage;
    private double discount;

    public HealthInsurancePlan(){

    }
    public HealthInsurancePlan(double coverage, double discount) {
        setCoverage(coverage);
        setDiscount(discount);
    }


    public double getCoverage() {
        return coverage;
    }

    public abstract double computeMonthlyPremium (double salary);

    public double getDiscount() {
        return discount;
    }

    private void setCoverage(double coverage) {
        this.coverage = coverage;
    }

    private void setDiscount(double discount) {
        this.discount = discount;
    }
}
