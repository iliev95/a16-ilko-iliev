package com.company;

public class PlatinumPlan extends HealthInsurancePlan{

    public PlatinumPlan(){
        super(0.9, 50);
    }

    @Override
    public double computeMonthlyPremium(double salary) {
        double premium =  salary * 0.08;
        return premium;
    }
}
