package com.company;

public class SilverPlan extends HealthInsurancePlan{

    public SilverPlan(){
        super(0.7,30);
    }

    @Override
    public double computeMonthlyPremium(double salary) {
        double premium = 0.06 * salary;
        return premium;
    }
}
