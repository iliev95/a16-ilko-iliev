package com.company;
import com.company.HealthInsurancePlan;

public class Billing {

    public static double[] computePymentAmount (Patient patient, double amount){
        double [] values = new double [2];

        if (!patient.isInsured()) {
            values[0] = 0;
            values[1] = amount;
        } else if (patient.getInsurancePlan() == null && patient.isInsured()) {
            values [0] = 0;
            values[1] = amount - 20;
        } else {
            values[0] = patient.getInsurancePlan().getCoverage() * amount;
            values [1] = amount - values[0] - patient.getInsurancePlan().getDiscount();
        }

        if (values[1] <0) {
            values[1] = 0;
        }

        return values;
    }
}
