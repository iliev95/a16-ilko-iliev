package com.company;

public class BlueCrossBlueShield implements InsuranceBrand{
    private long id;
    private String name;

    public String getName() {
        return name;
    }

    public long getId() {
        return id;
    }

    private void setName(String name) {
        this.name = name;
    }

    private void setId(long id) {
        this.id = id;
    }

    @Override
    public double computeMonthlyPremium(HealthInsurancePlan insurancePlan, int age, boolean smoking) {
        double addToPremium = 0;
        if (insurancePlan instanceof PlatinumPlan) {
            if (age > 55) {
                addToPremium += 200;
            }
            if (smoking) {
                addToPremium += 100;
            }
        } else if (insurancePlan instanceof GoldPlan) {
            if (age > 55) {
                addToPremium += 150;
            }
            if (smoking) {
                addToPremium += 90;
            }
        } else if (insurancePlan instanceof SilverPlan) {
            if (age > 55) {
                addToPremium += 100;
            }
            if (smoking) {
                addToPremium += 80;
            }
        } else if (insurancePlan instanceof BronzePlan) {
            if (age > 55) {
                addToPremium += 50;
            }
            if (smoking) {
                addToPremium += 70;
            }
        }

        return addToPremium;
    }
}
