package com.company;

public class BronzePlan extends HealthInsurancePlan{

    public BronzePlan(){
        super(0.6,25);
    }

    @Override
    public double computeMonthlyPremium(double salary, int age, boolean smoking) {
        double premium =  salary * 0.05;
        premium+= getOfferedBy().computeMonthlyPremium(this, age, smoking );
        return premium;
    }
}
