package com.company;

public class GoldPlan extends HealthInsurancePlan{

    public GoldPlan (){
        super(0.8,40);
    }

    @Override
    public double computeMonthlyPremium(double salary, int age, boolean smoking) {
        double premium =  salary * 0.07;
        premium+= getOfferedBy().computeMonthlyPremium(this, age, smoking );
        return premium;
    }
}
