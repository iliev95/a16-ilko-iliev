package  com.company;

public abstract class HealthInsurancePlan{
    private double coverage;
    private double discount;
    private InsuranceBrand offeredBy;

    public HealthInsurancePlan(){

    }
    public HealthInsurancePlan(double coverage, double discount) {
        setCoverage(coverage);
        setDiscount(discount);
    }


    public InsuranceBrand getOfferedBy() {
        return offeredBy;
    }


    public double getCoverage() {
        return coverage;
    }

    public abstract double computeMonthlyPremium(double salary, int age, boolean smoking);

    public double getDiscount() {
        return discount;
    }

    private void setCoverage(double coverage) {
        this.coverage = coverage;
    }

    private void setDiscount(double discount) {
        this.discount = discount;
    }

    protected void setOfferedBy(InsuranceBrand offeredBy) {
        this.offeredBy = offeredBy;
    }
}
