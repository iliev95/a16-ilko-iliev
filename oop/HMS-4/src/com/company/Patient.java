package com.company;

public class Patient extends User{

    public Patient(){

    }

    public Patient(long id, String firstName, String lastName, String gender, String email, boolean insured ){
        super(id, firstName, lastName, gender, email, insured);
    }

    public Patient(long id, String firstName, String lastName, String gender, String email, boolean insured,
                   HealthInsurancePlan insurancePlan ){
        super(id, firstName, lastName, gender, email, insured, insurancePlan);
    }
}
