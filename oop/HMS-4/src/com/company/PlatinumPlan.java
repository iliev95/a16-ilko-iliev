package com.company;

public class PlatinumPlan extends HealthInsurancePlan{

    public PlatinumPlan(){
        super(0.9, 50);
    }

    @Override
    public double computeMonthlyPremium(double salary, int age, boolean smoking) {
        double premium =  salary * 0.08;
        premium+= getOfferedBy().computeMonthlyPremium(this, age, smoking);
        return premium;
    }
}
