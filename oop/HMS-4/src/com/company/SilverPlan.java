package com.company;

public class SilverPlan extends HealthInsurancePlan{

    public SilverPlan(){
        super(0.7,30);
    }

    @Override
    public double computeMonthlyPremium(double salary, int age, boolean smoking) {
        double premium =  salary * 0.06;
        premium+= getOfferedBy().computeMonthlyPremium(this, age, smoking );
        return premium;
    }
}
