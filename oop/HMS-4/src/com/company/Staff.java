package com.company;

public class Staff extends User {
    private int yearsOfExperience;
    private String description;
    private double salary;

    public Staff(){

    }

    public Staff(long id,String firstName, String lastName,
                 String gender, String email, int yearsOfExperience, String description, double salary,
                 boolean insured, HealthInsurancePlan insurancePlan){
        super( id,firstName,lastName,gender,email, insured, insurancePlan );
        setYearsOfExperience(yearsOfExperience);
        setDescription(description);
        setSalary(salary);
    }

    public int getYearsOfExperience() {
        return yearsOfExperience;
    }

    public String getDescription() {
        return description;
    }

    public double getSalary() {
        return salary;
    }

    private void setYearsOfExperience(int yearsOfExperience) {
        this.yearsOfExperience = yearsOfExperience;
    }

    private void setDescription(String description) {
        this.description = description;
    }

    private void setSalary(double salary) {
        this.salary = salary;
    }
}
