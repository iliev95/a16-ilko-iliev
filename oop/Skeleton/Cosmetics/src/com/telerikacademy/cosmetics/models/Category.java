package com.telerikacademy.cosmetics.models;
import com.telerikacademy.cosmetics.models.products.Product;

import java.util.ArrayList;
import java.util.List;

public class Category {
    private static final int MIN_NAME_LENGTH  = 3;
    private static final int MAX_NAME_LENGTH  = 15;
    private String name;
    private List<Product> products;

    public Category(String name) {
        setName(name);
        this.products = new ArrayList<>();
    }

    public List<Product> getProducts() {
        return new ArrayList<>(products);
    }

    public void addProduct(Product product) {
        boolean check = Validations.validateNullPointer(product);
        if (check) {
            throw new IllegalArgumentException();
        }
        products.add(product);
    }

    public void removeProduct(Product product) {
        if (!products.contains(product)) {
            throw new IllegalArgumentException("");
        }
        products.remove(product);
    }

    private void setName (String name){
        boolean check = Validations.validate(name, MIN_NAME_LENGTH, MAX_NAME_LENGTH);
        if(!check) {
            throw new IllegalArgumentException("");
        }

        this.name= name;

    }

    public String print(){
        String result = "#Category: " + name + "\n";
        if(products.size() == 0) {
            result += " #No product in this category";
        }
        for (int i = 0; i <products.size() ; i++) {
            result+= products.get(i).print();
        }

        return result;
    }
}
