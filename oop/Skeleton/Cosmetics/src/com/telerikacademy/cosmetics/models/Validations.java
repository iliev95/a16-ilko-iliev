
package com.telerikacademy.cosmetics.models;

import com.telerikacademy.cosmetics.models.products.Product;

public class Validations {

    public static boolean validateNullPointer (Product product){
        boolean result = false;
        if (product == null) {
            result = true;

        }
        return result;
    }

    public static boolean validate (String var, int min, int max){
        boolean validated = false;
        if (var.length() < min || var.length() > max ) {
            validated = false;
        } else {
            validated = true;
        }
        return validated;
    }
}
