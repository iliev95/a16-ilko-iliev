package com.telerikacademy.cosmetics.models.cart;
import com.telerikacademy.cosmetics.models.Validations;
import com.telerikacademy.cosmetics.models.products.Product;
import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
    private List<Product> productList;

    public ShoppingCart() {
        this.productList = new ArrayList<>();
    }

    public List<Product> getProductList() {

        return new ArrayList<>(productList);
    }

    public void addProduct(Product product) {
        boolean check = Validations.validateNullPointer(product);
        if (check) {
           throw new IllegalArgumentException();
        }
        productList.add(product);
    }

    public void removeProduct(Product product) {
        boolean check = Validations.validateNullPointer(product);
        if (check) {
            throw new IllegalArgumentException("");
        }
        productList.remove(product);
    }

    public boolean containsProduct(Product product) {
        boolean result = false;
        boolean check = Validations.validateNullPointer(product);
        if (check) {
          throw new IllegalArgumentException();
        }
        if (!productList.contains(product)) {
            result = false;
        }else {
            result = true;
        }
        return result;
    }

    public double totalPrice() {
        double total = 0;
        for (int i = 0; i <productList.size() ; i++) {
            total+= productList.get(i).getPrice();
        }
        return total;
    }
}
