package com.telerikacademy.cosmetics.models.products;
import com.telerikacademy.cosmetics.models.Validations;
import com.telerikacademy.cosmetics.models.common.GenderType;

public class Product {
    private static final double MIN_PRICE = 0;
    private static final int MIN_NAME_LENGTH = 3;
    private static final int MAX_NAME_LENGTH = 10;
    private static final int MIN_BRAND_LENGTH = 2;
    private static final int MAX_BRAND_LENGTH = 10;
    private double price;
    private String name;
    private String brand;
    private GenderType gender;

    public Product(String name, String brand, double price, GenderType gender) {
        setPrice(price);
        setName(name);
        setBrand(brand);
        this.gender = gender;
    }

    private void setPrice(double price) {
        if (price < MIN_PRICE) {
            throw new IllegalArgumentException("Price must be positive!");
        }
        this.price = price;
    }

    private void setName(String name) {
        boolean check  = Validations.validate(name, MIN_NAME_LENGTH, MAX_NAME_LENGTH);
        if (!check) {
            throw new IllegalArgumentException();
        }
        this.name = name;
    }

    private void setBrand(String brand) {
        boolean check = Validations.validate(brand, MIN_BRAND_LENGTH, MAX_BRAND_LENGTH);
        if (!check) {
            throw new IllegalArgumentException();
        }
        this.brand = brand;
    }

    public double getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public String getBrand() {

        return brand;
    }

    public String print() {
        return "#" + name + " " + brand + "\n" + "#" + "Price:" + " $" + price
                + "\n" + "#Gender:" + " " + gender + "\n" + "===";
    }
}
