package com.telerikacademy.cosmetics.models;

import java.util.List;

public class Validations {

    public static void validateListNullPointer (List<String> ingredients){
        if (ingredients == null) {
            throw new IllegalArgumentException("Invalid input.");
        }
    }

    public static void validateStringNullPointer (String str){
        if (str == null) {
            throw new IllegalArgumentException("Invalid input.");
        }
    }

    public static void validateStringLength (String name, int min, int max){
        if (name.length() > max || name.length() < min) {
            throw new IllegalArgumentException("Invalid input.");
        }
    }
}
