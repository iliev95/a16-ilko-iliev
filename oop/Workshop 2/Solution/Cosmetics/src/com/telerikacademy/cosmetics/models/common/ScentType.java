package com.telerikacademy.cosmetics.models.common;

public enum ScentType {
    LAVENDER {
        public String toString() {
            return "Lavender";
        }
    },
    VANILLA {
        public String toString() {
            return "Vanilla";
        }
    },
    ROSE {
        public String toString() {
            return "Rose";
        }
    }
}
