package com.telerikacademy.cosmetics.models.common;

public enum UsageType {
    EVERY_DAY {
        public String toString() {
            return "EveryDay";
        }
    },
    MEDICAL {
        public String toString() {
            return "Medical";
        }
    }
}
