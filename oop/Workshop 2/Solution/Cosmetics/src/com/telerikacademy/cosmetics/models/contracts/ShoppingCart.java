package com.telerikacademy.cosmetics.models.contracts;

import java.util.List;

public interface ShoppingCart {
    double totalPrice();

    List<Product> getProductList();

    void removeProduct(Product product);

    boolean containsProduct(Product product);

    void addProduct(Product product);
}
