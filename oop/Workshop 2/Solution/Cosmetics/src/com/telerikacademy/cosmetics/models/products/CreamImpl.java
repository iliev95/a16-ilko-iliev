package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.ScentType;
import com.telerikacademy.cosmetics.models.contracts.Cream;

public class CreamImpl extends ProductImpl implements Cream {
    private ScentType scent;

    public CreamImpl(String name, String brand, double price, GenderType gender, ScentType scent){
        super(name, brand, price, gender);
        setScent(scent);
    }

    private void setScent(ScentType scent) {
        this.scent = scent;
    }

    @Override
    public ScentType getScentType() {
        return scent;
    }

    @Override
    public String print() {
        return String.format("#%s %s%n #Price: $%f%n #Gender: %s%n #Scent: %s%n ===", super.getName(),
                super.getBrand(), super.getPrice(), String.valueOf(super.getGender().toString()), String.valueOf(scent.toString()));
    }
}
