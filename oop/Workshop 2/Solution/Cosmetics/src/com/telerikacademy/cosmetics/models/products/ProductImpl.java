package com.telerikacademy.cosmetics.models.products;
import com.telerikacademy.cosmetics.models.Validations;
import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Product;

abstract class ProductImpl implements Product {
    private static final int MIN_NAME_LENGTH = 3;
    private static final int MAX_NAME_LENGTH = 10;
    private static final int MIN_BRAND_LENGTH = 2;
    private static final int MAX_BRAND_LENGTH = 10;
    private static final int MIN_CREAM_NAME = 3;
    private static final int MAX_CREAM_NAME = 15;
    private static final int MIN_CREAM_BRAND = 3;
    private static final int MAX_CREAM_BRAND = 15;
    private static final String INVALID_PRICE = "Price cannot be negative.";

    private String name;
    private String brand;
    private double price;
    private GenderType gender;

    public ProductImpl(String name, String brand, double price, GenderType gender){
        setName(name);
        setBrand(brand);
        setPrice(price);
        setGender(gender);

    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getBrand() {
        return brand;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public GenderType getGender() {
        return gender;
    }

    private void setName(String name) {
        Validations.validateStringNullPointer(name);
        if (this instanceof CreamImpl) {
            Validations.validateStringLength(name,MIN_CREAM_NAME, MAX_CREAM_NAME);
        } else {
            Validations.validateStringLength(name, MIN_NAME_LENGTH, MAX_NAME_LENGTH);
        }

        this.name = name;
    }

    private void setBrand(String brand) {
        Validations.validateStringNullPointer(brand);
        if (this instanceof CreamImpl) {
            Validations.validateStringLength(brand, MIN_CREAM_BRAND, MAX_CREAM_BRAND);
        } else {
            Validations.validateStringLength(brand, MIN_BRAND_LENGTH, MAX_BRAND_LENGTH);
        }

        this.brand = brand;
    }

    private void setPrice(double price) {
        if (price <0) {
            throw new IllegalArgumentException(INVALID_PRICE);
        }
        this.price = price;
    }

    private void setGender(GenderType gender) {
        this.gender = gender;
    }

    public abstract String print();
}
