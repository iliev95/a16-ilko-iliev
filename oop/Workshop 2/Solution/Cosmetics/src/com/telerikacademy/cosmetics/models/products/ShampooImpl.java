package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.Validations;
import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.Product;
import com.telerikacademy.cosmetics.models.contracts.Shampoo;

import java.util.Arrays;

public class ShampooImpl extends ProductImpl implements Shampoo {
    private static final String INVALID_MILILITERS = "Mililiters cannot be negative.";
    private int mililiters;
    private UsageType usageType;


    public ShampooImpl(String name, String brand, double price, GenderType gender, int mililiters, UsageType usageType) {
        super(name, brand, price, gender);
        setMililiters(mililiters);
        setUsageType(usageType);
    }

    private void setMililiters(int mililiters) {
        if (mililiters < 0) {
            throw new IllegalArgumentException(INVALID_MILILITERS);
        }
        this.mililiters = mililiters;
    }
    
    @Override
    public int getMililiters() {
        return mililiters;
    }

     private void setUsageType(UsageType usageType) {
        this.usageType = usageType;
    }

    @Override
    public UsageType getUsageType() {
        return usageType;
    }

    @Override
    public String print() {
        return String.format("#%s %s%n #Price: $%f%n #Gender: %s%n #Milliliters: %d%n #Usage: %s%n ===", super.getName(),
                super.getBrand(), super.getPrice(), String.valueOf(super.getGender().toString()), mililiters, String.valueOf(usageType.toString()));
    }
}
