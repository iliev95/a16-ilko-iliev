package com.telerikacademy.cosmetics.models.products;
import com.telerikacademy.cosmetics.models.Validations;
import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Toothpaste;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ToothpasteImpl extends ProductImpl implements Toothpaste {
    private List<String> ingredients;

    public ToothpasteImpl(String name, String brand, double price, GenderType gender, List<String> ingredients) {
        super(name, brand, price, gender);
        setIngredients(ingredients);
    }

    @Override
    public List<String> getIngredients() {
        return new ArrayList<>(ingredients) ;
    }

    private void setIngredients(List<String> ingredients) {
        Validations.validateListNullPointer(ingredients);
        this.ingredients = new ArrayList<>(ingredients);
    }

    @Override
    public String print() {
        return String.format("#%s %s%n #Price: $%f%n #Gender: %s%n #Ingrdients: %s%n ===", super.getName(),
                super.getBrand(), super.getPrice(), String.valueOf(super.getGender().toString()), Arrays.toString(ingredients.toArray()));
    }
}
