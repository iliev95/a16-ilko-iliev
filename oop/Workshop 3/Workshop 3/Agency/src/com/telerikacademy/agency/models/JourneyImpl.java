package com.telerikacademy.agency.models;
import com.telerikacademy.agency.models.Validator;
import com.telerikacademy.agency.models.contracts.Journey;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

import javax.swing.*;

public class JourneyImpl implements Journey {
    private static final int MIN_STARTLENGTH = 5;
    private static final int MAX_STARTLENGTH = 25;
    private static final int MIN_DESTINATIONTLENGTH = 5;
    private static final int MAX_DESTINATIONLENGTH = 25;
    private static final int MIN_DISTANCE = 5;
    private static final int MAX_DISTANCE = 5000;
    private static final String START_MESSAGE = "The StartingLocation's length cannot be less than 5 or more than 25 symbols long.";
    private static final String DESTINATION_MESSAGE ="The Destination's length cannot be less than 5 or more than 25 symbols long.";
    private static final String DISTANCE_MESSAGE = "The Distance cannot be less than 5 or more than 5000 kilometers.";

    private String startLocation;
    private String destination;
    private int distance;
    private Vehicle vehicle;

    public JourneyImpl(String startLocation, String destination, int distance, Vehicle vehicle){
        setStartLocation(startLocation);
        setDestination(destination);
        setDistance(distance);
        setVehicle(vehicle);
    }

    @Override
    public int getDistance() {
        return distance;
    }

    @Override
    public String getDestination() {
        return destination;
    }

    @Override
    public String getStartLocation() {
        return startLocation;
    }

    @Override
    public Vehicle getVehicle() {
        return vehicle;
    }

    private void setDestination(String destination){
        Validator.checkNull(destination);
        if (!Validator.checkStirngLength(destination, MIN_DESTINATIONTLENGTH, MAX_DESTINATIONLENGTH)) {
            throw new IllegalArgumentException(DESTINATION_MESSAGE);
        }
        this.destination = destination;
    }

    private void setDistance(int distance) {
        if (!Validator.checkParameters(distance, MIN_DISTANCE, MAX_DISTANCE)) {
            throw new IllegalArgumentException(DISTANCE_MESSAGE);
        }

        this.distance = distance;
    }

    private void setStartLocation(String startLocation) {
        Validator.checkNull(startLocation);
        if (!Validator.checkStirngLength(startLocation, MIN_STARTLENGTH, MAX_STARTLENGTH)) {
            throw new IllegalArgumentException(START_MESSAGE);
        }
        this.startLocation = startLocation;
    }

    private void setVehicle(Vehicle vehicle) {
        Validator.checkNull(vehicle);
        this.vehicle = vehicle;
    }

    @Override
    public double calculateTravelCosts() {
        return distance * vehicle.getPricePerKilometer();
    }

    @Override
    public String toString() {
        return String .format("Journey ----\nStart location: %s\nDestination: " +
                        "%s\nDistance: %d\nVehicle type: %s\nTravel costs: %.2f\n"
                , startLocation,
                destination,
                distance, vehicle.getType(), calculateTravelCosts());
    }
}
