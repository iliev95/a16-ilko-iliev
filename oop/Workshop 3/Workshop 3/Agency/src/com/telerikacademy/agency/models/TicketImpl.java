package com.telerikacademy.agency.models;

import com.telerikacademy.agency.models.contracts.Journey;
import com.telerikacademy.agency.models.contracts.Ticket;

public class TicketImpl implements Ticket {
    private Journey journey;
    private double administrativeCosts;

    public TicketImpl(Journey journey, double administrativeCosts){
        setJourney(journey);
        setAdministrativeCosts(administrativeCosts);
    }

    @Override
    public double getAdministrativeCosts() {
        return administrativeCosts;
    }

    @Override
    public Journey getJourney() {
        return journey;
    }

    private void setAdministrativeCosts(double administrativeCosts) {
        this.administrativeCosts = administrativeCosts;
    }

    private void setJourney(Journey journey) {
        this.journey = journey;
    }

    @Override
    public double calculatePrice() {
        return administrativeCosts * journey.calculateTravelCosts();
    }

    @Override
    public String toString() {
        return String.format("Ticket ----\nDestination: %s\nPrice: %.2f\n", journey.getDestination(), calculatePrice());
    }
}
