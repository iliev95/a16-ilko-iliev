package com.telerikacademy.agency.models;

public class Validator {

    public static  void checkNull(Object object){
        if (object == null){
            throw new IllegalArgumentException();
        }
    }

    public static boolean checkStirngLength(String str, int min, int max){
        boolean check = true;
        if (str.length() > max || str.length() < min ) {
            check = false;
        }
        return check;
    }

    public static boolean checkParameters(int number, int min, int max){
        boolean check = true;
        if (number< min || number > max) {
            check = false;
        }
        return check;
    }

    public static boolean checkDoubleParameters(double number, double min , double max){
        boolean check = true;

        if (number < min || number > max) {
            check = false;
        }
        return check;
    }

}
