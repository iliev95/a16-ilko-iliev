package com.telerikacademy.agency.models.common;

public enum VehicleType {

    Land ("LAND"),
    Air("AIR"),
    Sea("SEA");

    private String output;

    VehicleType(String output){
        this.output = output;
    }

    @Override
    public String toString() {
        return output;
    }
}
