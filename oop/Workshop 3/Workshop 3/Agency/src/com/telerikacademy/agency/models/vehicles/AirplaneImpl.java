package com.telerikacademy.agency.models.vehicles;
import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Airplane;

public class AirplaneImpl extends VehicleImpl implements Airplane {
    private static final int MIN_CAPACITY = 1;
    private static final int MAX_CAPACITY = 800;
    private static final String SPEC_ERROR_MESSAGE = "A vehicle with less than 1 passengers or more than 800 passengers cannot exist!";
    private static final VehicleType  type = VehicleType.Air;
    private boolean hasFreeFood;


    public AirplaneImpl(int passengerCapacity, double pricePerKilometer, boolean hasFreeFood) {
        super(passengerCapacity, pricePerKilometer);
        setHasFreeFood(hasFreeFood);
    }

    @Override
    public boolean hasFreeFood() {
        return false;
    }

    @Override
    protected int getMinCapacity() {
        return MIN_CAPACITY;
    }

    @Override
    protected int getMaxCapacity() {
         return MAX_CAPACITY;
    }

    @Override
    protected String getSpecErrorMessage() {
        return SPEC_ERROR_MESSAGE;
    }

    @Override
    public VehicleType getType() {
        return type;
    }

    private void setHasFreeFood(boolean hasFreeFood) {
        this.hasFreeFood = hasFreeFood;
    }

    @Override
    public String toString() {
        return String.format("Airplane ----\n%s\nHas free food: %s\n", super.toString(),String.valueOf(hasFreeFood));
    }
}
