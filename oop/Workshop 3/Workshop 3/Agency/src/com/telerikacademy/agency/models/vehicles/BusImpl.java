package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Bus;

public class BusImpl extends VehicleImpl implements Bus {
    private static final int MIN_CAPACITY = 10;
    private static final int MAX_CAPACITY = 50;
    private static final VehicleType type = VehicleType.Land;
    private static final String SPEC_ERROR_MESSAGE = "A bus cannot have less than 10 passengers or more than 50 passengers.";

    public BusImpl(int passengerCapacity, double pricePerKilometer) {
        super(passengerCapacity, pricePerKilometer);
    }

    @Override
    protected int getMinCapacity() {
        return MIN_CAPACITY;
    }

    @Override
    protected int getMaxCapacity() {
        return MAX_CAPACITY;
    }

    @Override
    protected String getSpecErrorMessage() {
        return SPEC_ERROR_MESSAGE;
    }

    @Override
    public VehicleType getType() {
        return type;
    }

    @Override
    public String toString() {
        return String.format("Bus ----\n%s\n", super.toString());
    }
}
