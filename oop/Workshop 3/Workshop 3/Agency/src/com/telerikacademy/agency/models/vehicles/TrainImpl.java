package com.telerikacademy.agency.models.vehicles;
import com.telerikacademy.agency.models.Validator;
import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Train;

public class TrainImpl extends VehicleImpl implements Train {
    private static final int MIN_CAPACITY = 30;
    private static final int MAX_CAPACITY = 150;
    private static final int MIN_CARTS = 1;
    private static final int MAX_CARTS = 15;
    private static final String CARTS_MESSAGE ="A train cannot have less than 1 cart or more than 15 carts.";
    private static final String SPEC_ERROR_MESSAGE = "A train cannot have less than 30 passengers or more than 150 passengers.";
    private static final VehicleType type = VehicleType.Land;
    private int carts;

    public TrainImpl(int passengerCapacity, double pricePerKilometer, int carts) {
        super(passengerCapacity, pricePerKilometer);
        setCarts(carts);
    }

    @Override
    public int getCarts() {
        return carts;
    }

    @Override
    protected String getSpecErrorMessage() {
        return SPEC_ERROR_MESSAGE;
    }

    @Override
    protected int getMinCapacity() {
        return MIN_CAPACITY;
    }

    @Override
    protected int getMaxCapacity() {
        return MAX_CAPACITY;
    }

    @Override
    public VehicleType getType() {
        return type;
    }

    private void setCarts(int carts) {
        if (!Validator.checkParameters(carts, MIN_CARTS, MAX_CARTS)) {
            throw new IllegalArgumentException(CARTS_MESSAGE);
        }
        this.carts = carts;
    }

    @Override
    public String toString() {
        return String.format("Train ----\n%s\nCarts amount: %d\n", super.toString(), getCarts());
    }
}
