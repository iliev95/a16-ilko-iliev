package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.Validator;
import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

public class VehicleImpl implements Vehicle {
    private static final int MIN_CAPACITY = 1;
    private static final int MAX_CAPACITY = 800;
    private static final double MIN_PRICE = 0.10;
    private static final double MAX_PRICE = 2.50;
    private static final String SPEC_ERROR_MESSAGE = "A vehicle with less than 1 passengers or more than 800 passengers cannot exist!";
    private static final String PRICE_MESSAGE = "A vehicle with a price per kilometer lower than $0.10 or higher than $2.50 cannot exist!";

    private int passengerCapacity;
    private VehicleType type;
    private double pricePerKilometer;

    public VehicleImpl(int passengerCapacity, double pricePerKilometer){
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
    }

    @Override
    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    @Override
    public VehicleType getType() {
        return type;
    }

    @Override
    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    protected int getMaxCapacity() {
        return MAX_CAPACITY;
    }

    protected int getMinCapacity() {
        return MIN_CAPACITY;
    }

    protected String getSpecErrorMessage() {
        return SPEC_ERROR_MESSAGE;
    }

    private void setPassengerCapacity(int passengerCapacity) {
        if (!Validator.checkParameters(passengerCapacity, MIN_CAPACITY, MAX_CAPACITY)) {
            throw new IllegalArgumentException(SPEC_ERROR_MESSAGE);
        }
        if (!Validator.checkParameters(passengerCapacity,getMinCapacity(), getMaxCapacity())) {
            throw new IllegalArgumentException(getSpecErrorMessage());
        }

        this.passengerCapacity = passengerCapacity;
    }

    private void setPricePerKilometer(double pricePerKilometer) {
        if (!Validator.checkDoubleParameters(pricePerKilometer, MIN_PRICE, MAX_PRICE)) {
            throw new IllegalArgumentException(PRICE_MESSAGE);
        }
        this.pricePerKilometer = pricePerKilometer;
    }

    private void setType(VehicleType type) {
        this.type = type;
    }


    @Override
    public String toString() {
        return String.format("Passenger capacity: %d \nPrice per Kilometer: %.2f \nVehicle type: %s",
                getPassengerCapacity(), getPricePerKilometer(), getType().toString());
    }
}
