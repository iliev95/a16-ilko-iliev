package com.telerikacademy.agency.models.vehicles.contracts;

public interface Bus extends Vehicle {
    // Even though this interface is empty, it acts as a reference type. For example in class CreteBusCommand - line 31.
    // In addition, it extends Vehicle, so all the needed functions are being inherited.
}
