package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.AdjustableChair;
import com.telerikacademy.furniture.models.enums.MaterialType;

public class AdjustableChairImpl extends ChairImpl implements AdjustableChair {


    public AdjustableChairImpl(String model, MaterialType materialType, double price, double height, int numberOfLegs, String type){
        super (model, materialType, price, height, numberOfLegs, type);
    }

    public AdjustableChairImpl(String model, MaterialType materialType, double price, double height, int numberOfLegs) {
        super(model, materialType, price, height, numberOfLegs);
    }



    @Override
    public void setHeight(double height) {
        if (height < 0) {
            throw new IllegalArgumentException();
        }
        super.setHeight(height);
    }

    @Override
    public String toString() {
        return String.format("%s", super.toString());
    }


}
