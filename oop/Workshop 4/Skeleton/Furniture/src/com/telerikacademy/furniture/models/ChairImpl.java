package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.Chair;
import com.telerikacademy.furniture.models.enums.MaterialType;

public class ChairImpl extends FurnitureImpl implements Chair {
    private int numberOfLegs;
    private String type;
    private static final String LEGS_ERROR = "Legs number cannot be negtive.";

    public ChairImpl (String model, MaterialType materialType, double price, double height, int numberOfLegs) {
        super(model, materialType, price, height);
        setNumberOfLegs(numberOfLegs);
        this.type = "Chair";
    }

    public ChairImpl (String model, MaterialType materialType, double price, double height, int numberOfLegs, String type) {
        super(model, materialType, price, height);
        setType(type);
        setNumberOfLegs(numberOfLegs);
    }


    private void setNumberOfLegs(int numberOfLegs) {
        if (numberOfLegs <= 0) {
            throw new IllegalArgumentException();
        }
        this.numberOfLegs = numberOfLegs;
    }

    @Override
    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    private void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }


    @Override
    public String toString() {
        return String.format("Type: %s, %s, Legs: %d", getType(), super.toString(), getNumberOfLegs());
    }
}
