package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.Company;
import com.telerikacademy.furniture.models.contracts.Furniture;

import java.util.ArrayList;
import java.util.List;

public class CompanyImpl implements Company {
    private String name;
    private String registrationNumber;
    private List<Furniture> furnitures;
    private static final String NO_FURNITURES = "no furnitures";
    private static final String ONE_FURNITURE =  "1 furniture";
    private static final int MIN_NAME_LENGTH = 5;

    public CompanyImpl(String name, String registrationNumber) {
        setName(name);
        setRegistrationNumber(registrationNumber);
        this.furnitures = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public List<Furniture> getFurnitures() {
        return new ArrayList<>(furnitures);
    }

    private void setName(String name) {
        Validator.checkNull(name);
        if (Validator.validateStringMin(name, MIN_NAME_LENGTH)) {
            throw new IllegalArgumentException();
        }
        this.name = name;
    }

    private void setRegistrationNumber(String registrationNumber) {
        if (Validator.checkRegNumber(registrationNumber)) {
            throw new IllegalArgumentException();
        }
        this.registrationNumber = registrationNumber;
    }

    public void add(Furniture furniture) {
        furnitures.add(furniture);
    }

    public String catalog() {
        if(furnitures.size() == 0) {
            System.out.printf("%s - %s - %s", getName(), getRegistrationNumber(), NO_FURNITURES);
        } else if (furnitures.size() == 1) {
            System.out.printf("%s - %s - %s\n", getName(), getRegistrationNumber(), ONE_FURNITURE);
        } else {
            System.out.printf("%s - %s - %d furnitures\n", getName(), getRegistrationNumber(), furnitures.size());
        }

        furnitures.sort((x1, x2) -> {
            int compareResult = (int) (x1.getPrice() - x2.getPrice());
            if (compareResult == 0) {
                compareResult = x1.getModel().compareTo(x2.getModel());
            }
            return compareResult;
        });

        StringBuilder strBuilder = new StringBuilder();
        for (Furniture furniture : furnitures) {
            strBuilder.append(furniture.toString());
            strBuilder.append(System.getProperty("line.separator"));
        }

        return strBuilder.toString().trim();
    }

    public Furniture find(String model) {
        Validator.checkNull(model);
        Furniture item = furnitures.stream()
                .filter(x -> x.getModel().equalsIgnoreCase(model))
                .findFirst()
                .orElse(null);

        return item;
    }

    public void remove(Furniture furniture) {
        if (furnitures.contains(furniture)) {
            furnitures.remove((furniture));
        }
    }
}
