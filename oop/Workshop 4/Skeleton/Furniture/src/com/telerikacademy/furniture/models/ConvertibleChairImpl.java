package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.ConvertibleChair;
import com.telerikacademy.furniture.models.enums.MaterialType;

public class ConvertibleChairImpl extends ChairImpl implements ConvertibleChair {
    private boolean converted;
    private double initialHeight;
    private String state;

    public ConvertibleChairImpl(String model, MaterialType materialType, double price, double height, int numberOfLegs, String type){
        super( model, materialType, price, height, numberOfLegs, type);
        this.initialHeight = super.getHeight();
        this.converted = false;
        this.state = "Normal";
    }

    public ConvertibleChairImpl(String model, MaterialType materialType, double price, double height, int numberOfLegs){
        super( model, materialType, price, height, numberOfLegs);
        this.initialHeight = super.getHeight();
        this.converted = false;
        this.state = "Normal";
    }


    @Override
    public void convert() {
        if (!converted) {
            this.converted = true;
            super.setHeight(0.10);
            this.state = "Normal";
        } else {
           this.converted = false;
           super.setHeight(initialHeight);
           this.state = "Converted" ;
        }
    }

    public String getState() {
        return state;
    }

    @Override
    public boolean getConverted() {
        return this.converted;
    }

    @Override
    public String toString() {
        return String.format("%s, State: %s", super.toString(), getState());
    }
}
