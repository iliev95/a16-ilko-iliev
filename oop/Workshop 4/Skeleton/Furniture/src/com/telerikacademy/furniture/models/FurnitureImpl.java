package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.Furniture;
import com.telerikacademy.furniture.models.enums.MaterialType;

public class FurnitureImpl implements Furniture {
    private String model;
    private MaterialType materialType;
    private double price;
    private double height;
    private static final int MIN_MODEL_LENGTH = 3;
    private static final String PRICE_ERROR = "Price cannot be negative";
    private static final String HEIGHT_ERROR = "Height cannot be negative.";

    public FurnitureImpl (String model, MaterialType materialType, double price, double height){
        setModel(model);
        setMaterialType(materialType);
        setPrice(price);
        setHeight(height);
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public double getHeight() {
        return height;
    }

    @Override
    public MaterialType getMaterialType() {
        return materialType;
    }

    @Override
    public String getModel() {
        return model;
    }

    private void setPrice(double price) {
        if(price <= 0) {
            throw new IllegalArgumentException(PRICE_ERROR);
        }
        this.price = price;
    }

    private void setModel(String model) {
        Validator.checkNull(model);
        if (Validator.validateStringMin(model, MIN_MODEL_LENGTH)) {
            throw new IllegalArgumentException();
        }
        this.model = model;
    }

    protected void setHeight(double height) {
        if (height <= 0) {
            throw new IllegalArgumentException(HEIGHT_ERROR);
        }
        this.height = height;
    }

    private void setMaterialType(MaterialType materialType) {
        this.materialType = materialType;
    }

    @Override
    public String toString() {
        return String.format("Model: %s, Material: %s, Price: %.2f, Height: %.2f",
                getModel(), getMaterialType().toString(), getPrice(), getHeight());
    }
}
