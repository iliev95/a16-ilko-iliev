package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.Furniture;
import com.telerikacademy.furniture.models.contracts.Table;
import com.telerikacademy.furniture.models.enums.MaterialType;

public class TableImpl extends FurnitureImpl implements Table {
    private double width;
    private double length;
    private static final String LENGTH_ERROR = "Length must be greater than 0.";
    private static final String WIDTH_ERROR = "Width must be greater than 0.";
    private final String type = "Table";

    public TableImpl(String model, MaterialType materialType, double price, double heigth, double width , double length){
        super(model, materialType, price, heigth);
        setLength(length);
        setWidth(width);
    }

    @Override
    public double getWidth() {
        return width;
    }

    @Override
    public double getLength() {
        return length;
    }



    @Override
    public double getArea() {
        return getLength() * getWidth();
    }

    private void setLength(double length) {
        if (length <= 0) {
            throw new IllegalArgumentException(LENGTH_ERROR);
        }
        this.length = length;
    }

    private void setWidth(double width) {
        if (width <= 0 ) {
           throw new IllegalArgumentException(WIDTH_ERROR);
        }
        this.width = width;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return String.format("Type: %s, %s, Length: %.2f , Width: %.2f, Area: %.4f",getType(), super.toString(),  getLength(), getWidth(), getArea());
    }
}
