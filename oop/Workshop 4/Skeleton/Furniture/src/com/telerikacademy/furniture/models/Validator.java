package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.Chair;

public class Validator {

    public static void checkNull (Object o){
        if (o == null) {
            throw new IllegalArgumentException();
        }
    }

    public static boolean validateStringMin (String str, int minLength){
        boolean result = false;
        if (str.length() < minLength) {
            result = true;
        }
        return result;
    }

    public static boolean checkRegNumber (String regNumber) {
        boolean result = false;
        if (regNumber.length() != 10) {
            result = true;
            return result;
        }
        for (int i = 0; i < regNumber.length(); i++) {
            char symbol = regNumber.charAt(i);
            if (!Character.isDigit(symbol)) {
                result = true;
                break;
            }
        }
        return result;
    }


}
