package com.telerikacademy.furniture.models.enums;

public enum MaterialType {
    WOODEN("Wooden"),
    LEATHER("Leather"),
    PLASTIC("Plastic");

    private String output;

    MaterialType(String output){
        this.output = output;
    }

    @Override
    public String toString() {
        return output;
    }
}
